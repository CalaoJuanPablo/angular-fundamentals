import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstLikeComponent } from './inst-like.component';

describe('InstLikeComponent', () => {
  let component: InstLikeComponent;
  let fixture: ComponentFixture<InstLikeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstLikeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstLikeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
