import { Component, Input } from '@angular/core';

@Component({
  selector: 'inst-like',
  templateUrl: './inst-like.component.html',
  styleUrls: ['./inst-like.component.css']
})
export class InstLikeComponent {

  @Input('likes-count') likesCount: number;
  @Input('is-like') isActive: boolean;

  onClick(){
    this.likesCount += (this.isActive) ? -1 : 1;
    this.isActive = !this.isActive
  }

}
