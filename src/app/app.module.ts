import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CourseComponent } from './course/course.component';
import { CoursesComponent } from './courses/courses.component';
import { FavoriteComponent } from './favorite/favorite.component';
import { InstFavComponent } from './inst-fav/inst-fav.component';
import { TitleCasingComponent } from './title-casing/title-casing.component';
import { TitleCasePipe } from './pipes/title-case.pipe';
import { MyLikeComponent } from './my-like/my-like.component';
import { LikeComponent } from './like/like.component';


@NgModule({
  declarations: [
    AppComponent,
    CourseComponent,
    CoursesComponent,
    FavoriteComponent,
    InstFavComponent,
    TitleCasingComponent,
    TitleCasePipe,
    MyLikeComponent,
    LikeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
