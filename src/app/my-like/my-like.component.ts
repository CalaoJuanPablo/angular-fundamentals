import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'my-like',
  templateUrl: './my-like.component.html',
  styleUrls: ['./my-like.component.css']
})
export class MyLikeComponent implements OnInit {

  constructor() { }

  @Input('is-liked') isLiked: boolean;
  @Input('likes-count') likesCount: number;
  @Output('click-like') likeClick = new EventEmitter();

  onClick(){
    this.isLiked = !this.isLiked;
    this.likeClick.emit((this.isLiked) ? this.likesCount++ : this.likesCount--);
  }


  ngOnInit() {
  }

}
