import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css']
})
export class FavoriteComponent implements OnInit {

  constructor() { }
  
  isFav = false;
  
  ngOnInit() {
  }
  
  favClick($event){
    this.isFav = !this.isFav;
  }

}
