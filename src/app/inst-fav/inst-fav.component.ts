import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

export interface FavoriteChangedEventArgs {
  newValue: boolean
}

@Component({
  selector: 'inst-fav',
  templateUrl: './inst-fav.component.html',
  styleUrls: ['./inst-fav.component.css']
})

export class InstFavComponent implements OnInit {

  constructor() { }

  @Input('is-favorite') isFavorite: boolean;
  @Output('change') click = new EventEmitter();

  ngOnInit() {
  }

  onClick(){
    this.isFavorite = !this.isFavorite;
    this.click.emit({ newValue: this.isFavorite });
  }

}
