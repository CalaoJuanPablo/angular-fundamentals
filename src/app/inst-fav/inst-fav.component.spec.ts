import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstFavComponent } from './inst-fav.component';

describe('InstFavComponent', () => {
  let component: InstFavComponent;
  let fixture: ComponentFixture<InstFavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstFavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstFavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
