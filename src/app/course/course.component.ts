import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {

  constructor() { }

  title = "List of Courses";
  courses = ["course 1", "course 2", "course 3"];
  textInside = "";

  onSave($event) {
    $event.stopPropagation();
    console.log("Button Clicked");
    this.keyUpEnter();
  }

  keyUpEnter(){
    console.log(`ENTER was pressed with text: ${this.textInside}`);
  }
  
  ngOnInit() {
  }

}
