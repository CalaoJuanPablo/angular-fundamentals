import { Component } from '@angular/core';
import { FavoriteChangedEventArgs } from './inst-fav/inst-fav.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  post = {
    title: "Title",
    isFavorite: false
  }

  tweet = {
    body: 'Here is the body of the tweet...',
    isLiked: false,
    likesCount: 10
  }

  onFavoriteChange(eventArgs: FavoriteChangedEventArgs) {
    console.log('Favorite Changed: ', eventArgs.newValue);
  }

  onLikeClick(likesCount){  }
}
